// Name: Xao Thao
// Date: 09/04/2019
// project name: functiondemo
// project description: understanding function (adding, dividing)





#include <iostream>
#include <conio.h>


using namespace std;


//function prototype
void sayHello(int count = 1); //"count" here is optional. "int" need to match the other function void sayHello 
							  // Adding a defaul value to it when adding default value to it we don't need to put anything into the main() sayHello() 
							  // function calls and goes back to where the code is being pulled from


int add(int a, int b);

bool divide(float num, float den, float &ans); // We sometime don't need the name variable 
											   // pass the answer by reference (&)

void countDownFrom(int number);

int main()
{
	sayHello(); // needs an integer to run how many loop in paranthesis
				// value within the () is optional if you don't have anything value for the parameters

	/*cout << add(3, 6);*/ // this is for the for loop void sayHello code

	//float num1 = 0; // good idea to get into the habit of putting 0 so that it can compile.
	//float num2 = 0;

	//cout << "Enter two numbers: ";
	//cin >> num1;
	//cin >> num2;

	//float answer = 0;
	//if (divide(num1, num2, answer))
	//{
	//	cout << num1 << " divided by " << num2 << " is " << answer;
	//}
	//else
	//{
	//	cout << "You cannot divide by zero.\n";

	//}

	//cout << " num1: " << num1 << "\n";
	//cout << " num2: " << num2 << "\n";
	
	// these code up here is for the bool

	countDownFrom(10); // has to call itself and has to have an end to be done

	char input = 'y';
	while (input == 'y')
	{
		countDownFrom(10);

		cout << "again?";
		cin >> input;
	}
	// this is how to code to try again because program will reclaim memory and retry code.


	/*cout << "again:";
	char input = 'n';
	cin >> input;
	if (input == 'y') << main();*/ // don't do this because the program does not have time to reclaim it's memory


	

	
	//_getch(); //if we have loop to continue program user inputting no will not close program so we don't need this when using loop to continue
	return 0;


}


void countDownFrom(int number)
{
	cout << number << "\n";
	if (number == 0) return;
	countDownFrom(number - 1);
	// or code can be written as:
	// if(number > 0) countDownFrom(number - 1);


}


int add(int a, int b)
{

	return a + b;

}

void sayHello(int count) //function //technically "int count" is a parameter 
{
	for (int i = 0; i < count; i++)
	{
		cout << "Hello!\n";
	}

}


bool divide(float num, float den, float &ans)
{
	if (den == 0) return false;

	ans = num / den;

	//by reference to indicate that they didn't change
	num++;
	den += 100;

	return true;
}

	





